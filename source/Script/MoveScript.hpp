/*
 * MoveScript.hpp
 *
 *  Created on: Mar 25, 2014
 *      Author: nikpmup
 */

#ifndef MOVESCRIPT_HPP_
#define MOVESCRIPT_HPP_

#include <Cupcake/Message/MsgHandler.hpp>
#include <Cupcake/BaseComponent/TransformComp.hpp>
#include <Cupcake/BaseComponent/TransformHelper.hpp>
#include <cstdio>

class MoveScript : public MsgHandler {
 public:
    MoveScript(TransformComp &comp) : mTrans(comp) {}
    GLvoid processMsg(Message &msg) {
        btVector3 pos = mTrans.mMatrix.getOrigin();
        pos.setY(pos.getY() + 1);
        mTrans.mMatrix.setOrigin(pos);
    }
 private:
    TransformComp &mTrans;

};

#endif /* MOVESCRIPT_HPP_ */
