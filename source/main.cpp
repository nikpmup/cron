// STD Libraries
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <thread>
// GL Libraries
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// Math Libraries
#include <glm/gtc/matrix_transform.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Map.hpp>
#include <Cupcake/BaseComponent/TransformComp.hpp>
#include <Cupcake/BaseComponent/TransformHelper.hpp>
#include <Cupcake/BaseComponent/ScaleComp.hpp>
#include <Cupcake/Entity/EntityManager.hpp>
#include <Cupcake/Device/DeviceSystem.hpp>
#include <Cupcake/Renderer/Render3DSystem.hpp>
#include <Cupcake/Renderer/Render2DSystem.hpp>
#include <Cupcake/Sound/SoundSystem.hpp>
#include <Cupcake/Message/MsgSystem.hpp>
// Libraries
#include <Factory/RenderFactory.hpp>
#include <Factory/PhysicsFactory.hpp>
#include <Factory/FontFactory.hpp>
#include <Factory/SoundFactory.hpp>
#include <Script/MoveScript.hpp>

#define WIDTH 1366
#define HEIGHT 768

static glm::vec3 pos(0.0f);
static glm::mat4 rot(1.0f);

GLvoid draw();

TransfList transList;
ScaleList scaleList;

DeviceSystem deviceSys(WIDTH, HEIGHT, "Entity System Test", draw);
EntityManager entityManager;
Render2DSystem render2D(deviceSys.getWidth(), deviceSys.getHeight());
MsgSystem msgSystem;
Render3DSystem render(transList, scaleList);
PhysicsSystem physSys(transList);
SoundSystem soundSys(transList);

//----------------------------------------------------------------------------
//
static GLvoid keyCallback(GLFWwindow* window, GLint key, GLint scancode,
                          GLint action, GLint mods) {
    if (key == GLFW_KEY_W && action == GLFW_PRESS) {
        Message msg;
        msgSystem.handleMessage("MOVE", msg);
    }
}

//----------------------------------------------------------------------------
//
GLvoid draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    render.update();
    render2D.update();
    soundSys.update();
    physSys.update();
}

//----------------------------------------------------------------------------
//
GLvoid generateBoxes(RenderFactory &factory, PhysicsFactory &phys,
                     SoundFactory &sound, GLuint number) {
    const GLuint SPACE = 10;
    const glm::vec3 SCALE(1.0f);
    const glm::mat4 ROT(1.0f);
    glm::vec3 pos(10.0f, 10.0f, -5.0f);

    for (GLuint idx = 0; idx < number; idx++) {
        Entity entity = entityManager.getNextId();
        factory.generateCube(entity, pos, SCALE, ROT);
        phys.generateCube(entity);
        sound.generateCube(entity);
        pos.y += SPACE * 2;
        pos.x -= SPACE * 2;
    }
}

//----------------------------------------------------------------------------
//
GLvoid releaseAll() {
    transList.releaseAll();
    scaleList.releaseAll();
    render.release();
    physSys.release();
    soundSys.release();
    render2D.release();
    deviceSys.release();
}

//----------------------------------------------------------------------------
//
GLint main(void) {
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    // Initializes Systems
    physSys.init(glm::vec3(0.0f, -10.0f, 0.0f), 60.0f);
    soundSys.init();
    render2D.init();
    deviceSys.setInput(keyCallback);

    // Creates Factory
    RenderFactory rendFactory(render, deviceSys);
    PhysicsFactory physFactory(physSys);
    SoundFactory soundFactory(soundSys);
    FontFactory fontFactory(render2D);

    // Generates "User"
    Entity listener = entityManager.getNextId();
    TransformComp *trans = TransformHelper::createTrans(pos, rot);
    transList.addEntry(listener, trans);
    MoveScript script(*trans);
    MsgHandler *msgHandle = &script;
    //Message testMsg;
    //msgHandle->processMsg(testMsg);
    msgSystem.subscribeHandler("MOVE", msgHandle);
    soundFactory.generateListener(listener);

    // Generates Boxes
    generateBoxes(rendFactory, physFactory, soundFactory, 2);

    // Generates Ground
    Entity entity = entityManager.getNextId();
    rendFactory.generateGround(entity, glm::vec3(0.0f, -2.0f, 0.0f),
                               glm::vec3(30.0f, 1.0f, 30.0f), glm::mat4(1.0f));
    physFactory.generatePlane(entity);
    soundFactory.generateBackground(entity);

    // Generates Text
    Entity text = entityManager.getNextId();
    fontFactory.generateText(text, "Font!", glm::vec2(0.0f, 32.0f));

    // Runs loop
    deviceSys.runLoop();

    releaseAll();
}
