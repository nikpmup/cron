/*
 * RenderFactory.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef RENDERFACTORY_HPP_
#define RENDERFACTORY_HPP_

// Cupcake Libraries
#include <Cupcake/Base/Factory.hpp>
#include <Cupcake/Renderer/Render3DSystem.hpp>
#include <Cupcake/Device/DeviceSystem.hpp>

class RenderFactory : public Factory<Render3DSystem> {

 public:
    RenderFactory(Render3DSystem &sys, DeviceSystem &devSys);

    enum eOBJ_LIST {
        CUBE,
        CONE,
        SPHERE,
        GROUND
    };

    enum ePROG_LIST {
        TEX_3D
    };

    enum eLIGHT_LIST {
        TOP
    };

    enum eCAM_LIST {
        PROJ,
        ISO
    };

    GLvoid generateEntity(Entity &entity, const glm::vec3 &pos,
                          const glm::vec3 &scale, const glm::mat4 &rotate,
                          eOBJ_LIST objType, eLIGHT_LIST lightType,
                          eCAM_LIST camType, ePROG_LIST progType);
    GLvoid generateEntity(Entity &entity, eOBJ_LIST objType,
                          eLIGHT_LIST lightType, eCAM_LIST camType,
                          ePROG_LIST progType);
    GLvoid generateCube(Entity &entity, const glm::vec3 &pos,
                        const glm::vec3 &scale, const glm::mat4 &rotate);
    GLvoid generateCube(Entity &entity);
    GLvoid generateGround(Entity &entity, const glm::vec3 &pos,
                          const glm::vec3 &scale, const glm::mat4 &rotate);
    GLvoid generateGround(Entity &entity);

 private:
    GLvoid initComponents();
    DeviceSystem &mDevSystem;
};

#endif /* RENDERFACTORY_HPP_ */
