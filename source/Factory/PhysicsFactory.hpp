/*
 * PhysicsFactory.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef PHYSICSFACTORY_HPP_
#define PHYSICSFACTORY_HPP_

// Cupcake Libraries
#include <Cupcake/Base/Factory.hpp>
#include <Cupcake/Physics/PhysicsSystem.hpp>
#include <Cupcake/Entity/EntityManager.hpp>

class PhysicsFactory : public Factory<PhysicsSystem> {

 public:
    PhysicsFactory(PhysicsSystem &sys);

    enum eSHAPE {
        CUBE,
        PLANE
    };

    GLvoid generateCube(Entity &entity);
    GLvoid generatePlane(Entity &entity);

 private:
    GLvoid initComponents();
};

#endif /* PHYSICSFACTORY_HPP_ */
