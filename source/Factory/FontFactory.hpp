/*
 * FontFactory.hpp
 *
 *  Created on: Mar 12, 2014
 *      Author: nikpmup
 */

#ifndef FONTFACTORY_HPP_
#define FONTFACTORY_HPP_

// GL Libraries
#include <GL/glew.h>
// Math Libraries
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Renderer/Render2DSystem.hpp>

class FontFactory {
 public:
    FontFactory(Render2DSystem &renderSys);

    GLvoid generateText(Entity &entity, const GLchar *text,
                        const glm::vec2 &pos);
 private:
    Render2DSystem &mRenderSys;

    GLvoid initComponent();
};

#endif /* FONTFACTORY_HPP_ */
