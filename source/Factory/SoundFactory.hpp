/*
 * SoundFactory.hpp
 *
 *  Created on: Mar 5, 2014
 *      Author: nikpmup
 */

#ifndef SOUNDFACTORY_HPP_
#define SOUNDFACTORY_HPP_

#include <Cupcake/Sound/SoundSystem.hpp>

class SoundFactory {
 public:
    SoundFactory(SoundSystem &sys);
    GLvoid generateCube(Entity &entity);
    GLvoid generateBackground(Entity &entity);
    GLvoid generateListener(Entity &entity);

 private:
    SoundSystem &mSoundSys;
};



#endif /* SOUNDFACTORY_HPP_ */
