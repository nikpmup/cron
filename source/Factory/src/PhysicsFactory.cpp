/*
 * PhysicsFactory.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Factory/PhysicsFactory.hpp>
// Cupcake Libraries
#include <Cupcake/Physics/ShapeCreator.hpp>
#include <Cupcake/Physics/BodyCreator.hpp>

//----------------------------------------------------------------------------
//
PhysicsFactory::PhysicsFactory(PhysicsSystem &sys)
        : Factory<PhysicsSystem>(sys) {
    initComponents();
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsFactory::initComponents() {
    mSystem.mShapeList.addEntry(CUBE,
                                ShapeCreator::createBox(1.0f, 1.0f, 1.0f));
    mSystem.mShapeList.addEntry(PLANE,
                                ShapeCreator::createBox(30.0f, 1.0f, 30.0f));
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsFactory::generateCube(Entity &entity) {
    const GLfloat MASS = 1.0f;
    const btTransform trans = mSystem.mTransfList.getEntry(entity)->mMatrix;
    btCollisionShape *shape = mSystem.mShapeList.getEntry(CUBE)->mShape;
    mSystem.addBody(entity, BodyCreator::createBody(MASS, trans, shape));
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsFactory::generatePlane(Entity &entity) {
    const GLfloat MASS = 0.0f;
    btTransform trans = mSystem.mTransfList.getEntry(entity)->mMatrix;
    btCollisionShape *shape = mSystem.mShapeList.getEntry(PLANE)->mShape;
    mSystem.addBody(entity, BodyCreator::createBody(MASS, trans, shape));
}
