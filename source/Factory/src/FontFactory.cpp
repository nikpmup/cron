/*
 * FontFactory.cpp
 *
 *  Created on: Mar 12, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Factory/FontFactory.hpp>

//----------------------------------------------------------------------------
//
FontFactory::FontFactory(Render2DSystem &renderSys)
        : mRenderSys(renderSys) {
    initComponent();
}

//----------------------------------------------------------------------------
//
GLvoid FontFactory::initComponent() {
    // Creates Program
    mRenderSys.installProgram(0, "resource/shader/fontVert.glsl",
                              "resource/shader/fontFrag.glsl");
    // Creates Atlas
    mRenderSys.createAtlas(0, "resource/font/arial.ttf", 32);
}

//----------------------------------------------------------------------------
//
GLvoid FontFactory::generateText(Entity &entity, const GLchar *text,
                                 const glm::vec2 &pos) {
    mRenderSys.createText(entity, 0, 0, text, pos,
                          glm::vec3(0.875f, 0.455f, 0.047f));
}
