/*
 * RenderFactory.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Factory/RenderFactory.hpp>
// Cupcake Libraries
#include <Cupcake/Camera/IsoCamera.hpp>
#include <Cupcake/BaseComponent/TransformHelper.hpp>
#include <Cupcake/Util/MeshLoader.hpp>
#include <Cupcake/Util/TextureLoader.hpp>
#include <Cupcake/Util/MaterialLoader.hpp>

//----------------------------------------------------------------------------
// Object Path
//----------------------------------------------------------------------------
enum eOBJ_COMP {
    OBJ_MESH,
    OBJ_TEXTURE,
    OBJ_MAT,
    OBJ_CNT
};

static const GLchar* const CubePath[] = { "resource/mesh/cube.obj",
        "resource/texture/cube.dds", "resource/mesh/cube.mtl" };

static const GLchar* const ConePath[] = { "resource/mesh/cone.obj",
        "resource/texture/cone.dds", "resource/mesh/cone.mtl" };

static const GLchar* const SpherePath[] = { "resource/mesh/sphere.obj",
        "resource/texture/sphere.dds", "resource/mesh/sphere.mtl" };

//----------------------------------------------------------------------------
// Program
//----------------------------------------------------------------------------
enum ePROG {
    VERT,
    FRAG
};

static const GLchar* const Tex3D[] = { "resource/shader/ltVert.glsl",
        "resource/shader/ltFrag.glsl" };

//----------------------------------------------------------------------------
// Light
//----------------------------------------------------------------------------
static LightComp *topLight = new LightComp(glm::vec3(0.0f, 20.0f, 0.0f),
                                           glm::vec3(1.0f, 1.0f, 1.0f), 2.0f);

//----------------------------------------------------------------------------
// Camera
//----------------------------------------------------------------------------
static CameraComp *projCam = new CameraComp();
static IsoCamera *isoCam = new IsoCamera();

//----------------------------------------------------------------------------
//
RenderFactory::RenderFactory(Render3DSystem &sys, DeviceSystem &devSys)
        : Factory<Render3DSystem>(sys),
          mDevSystem(devSys) {
    initComponents();
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::initComponents() {
    const GLfloat SIZE = 30.0f;
    const GLfloat ISOWIDTH = mDevSystem.getRatio() * SIZE;
    // Initializes mesh
    mSystem.mMeshList.addEntry(CUBE, MeshLoader::loadObj(CubePath[OBJ_MESH]));
    mSystem.mMeshList.addEntry(CONE, MeshLoader::loadObj(ConePath[OBJ_MESH]));
    mSystem.mMeshList.addEntry(SPHERE,
                               MeshLoader::loadObj(SpherePath[OBJ_MESH]));
    // Initializes texture
    mSystem.mTexList.addEntry(CUBE,
                              TextureLoader::loadDDS(CubePath[OBJ_TEXTURE]));
    mSystem.mTexList.addEntry(CONE,
                              TextureLoader::loadDDS(ConePath[OBJ_TEXTURE]));
    mSystem.mTexList.addEntry(SPHERE,
                              TextureLoader::loadDDS(SpherePath[OBJ_TEXTURE]));
    mSystem.mTexList.addEntry(
            GROUND, TextureLoader::loadDDS("resource/texture/ground.dds"));
    // Intializes material
    mSystem.mMatList.addEntry(CUBE,
                              MaterialLoader::loadMaterial(CubePath[OBJ_MAT]));
    mSystem.mMatList.addEntry(CONE,
                              MaterialLoader::loadMaterial(ConePath[OBJ_MAT]));
    mSystem.mMatList.addEntry(
            SPHERE, MaterialLoader::loadMaterial(SpherePath[OBJ_MAT]));
    // Intalls Program
    mSystem.installProgram(TEX_3D, Tex3D[VERT], Tex3D[FRAG]);
    // Intalls Light
    mSystem.mLightList.addEntry(TOP, topLight);
    // Initializes Camera
    projCam->setProjMat(45.0f, mDevSystem.getRatio(), 1.0f, 1000.0f);
    projCam->setViewMat(glm::vec3(0.0f, 0.0f, 0.0f),
                        glm::vec3(0.0f, 0.0f, -1.0f),
                        glm::vec3(0.0f, 1.0f, 0.0f));
    isoCam->setProjMat(-ISOWIDTH, ISOWIDTH, -SIZE, SIZE, -2 * SIZE, 2 * SIZE);
    mSystem.mCamList.addEntry(PROJ, projCam);
    mSystem.mCamList.addEntry(ISO, isoCam);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateEntity(Entity &entity, const glm::vec3 &pos,
                                     const glm::vec3 &scale,
                                     const glm::mat4 &rotate, eOBJ_LIST objType,
                                     eLIGHT_LIST lightType, eCAM_LIST camType,
                                     ePROG_LIST progType) {
    mSystem.mTransfList.addEntry(
            entity, TransformHelper::createTrans(pos, rotate));
    mSystem.mScaleList.addEntry(entity, new ScaleComp(scale));
    mSystem.addRenderable(entity, progType, objType, objType, objType, camType,
                          lightType);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateEntity(Entity &entity, eOBJ_LIST objType,
                                     eLIGHT_LIST lightType, eCAM_LIST camType,
                                     ePROG_LIST progType) {
    mSystem.addRenderable(entity, progType, objType, objType, objType, camType,
                          lightType);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateCube(Entity &entity, const glm::vec3 &pos,
                                   const glm::vec3 &scale,
                                   const glm::mat4 &rot) {
    mSystem.mTransfList.addEntry(
            entity, TransformHelper::createTrans(pos, rot));
    mSystem.mScaleList.addEntry(entity, new ScaleComp(scale));
    mSystem.addRenderable(entity, TEX_3D, CUBE, CUBE, CUBE, ISO, TOP);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateCube(Entity &entity) {
    mSystem.addRenderable(entity, TEX_3D, CUBE, CUBE, CUBE, ISO, TOP);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateGround(Entity &entity, const glm::vec3 &pos,
                                     const glm::vec3 &scale,
                                     const glm::mat4 &rotate) {
    mSystem.mTransfList.addEntry(
            entity, TransformHelper::createTrans(pos, rotate));
    mSystem.mScaleList.addEntry(entity, new ScaleComp(scale));
    mSystem.addRenderable(entity, TEX_3D, CUBE, CUBE, GROUND, ISO, TOP);
}

//----------------------------------------------------------------------------
//
GLvoid RenderFactory::generateGround(Entity &entity) {
    mSystem.addRenderable(entity, TEX_3D, CUBE, CUBE, GROUND, ISO, TOP);
}
