/*
 * SoundFactory.cpp
 *
 *  Created on: Mar 5, 2014
 *      Author: nikpmup
 */

// Header Function
#include <Factory/SoundFactory.hpp>
// Math Libraries
#include <glm/gtc/matrix_transform.hpp>

//----------------------------------------------------------------------------
//
SoundFactory::SoundFactory(SoundSystem &sys)
        : mSoundSys(sys) {
}

//----------------------------------------------------------------------------
//
GLvoid SoundFactory::generateCube(Entity &entity) {
    mSoundSys.addStream(entity, "resource/sound/warpy.wav");
    mSoundSys.playSound(entity, FMOD_LOOP_NORMAL, 4, 10000, 1.0f);
}

//----------------------------------------------------------------------------
//
GLvoid SoundFactory::generateBackground(Entity &entity) {
    mSoundSys.addStream(entity, "resource/sound/swell-pad.wav");
    mSoundSys.playSound(entity, FMOD_LOOP_NORMAL, 4, 10000, 1.0f);
}

//----------------------------------------------------------------------------
//
GLvoid SoundFactory::generateListener(Entity &entity) {
    mSoundSys.addListener(entity);
}
