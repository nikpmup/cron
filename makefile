# Finds Operating system
OS := $(shell uname -s)
NPROCS:= 1

# Compiler settings
CC = g++
CCFLAGS = -Wall -O3 -Werror -std=c++0x
DD_FLAGS = -DGL_GLEXT_PROTOTYPES -DGLEW_STATIC

# Folders
ROOT_DIR = .
SRC_NAME = source
OUT_NAME = bin
FIND = $(shell find $(1) -type d -print)
EXCLUDE := $(call FIND, "./bin") $(call FIND, "./.git") \
	$(call FIND, "./.settings") $(call FIND, "./resource")

# Include files
HDR_PATH = ../support/include
FT_PATH = /usr/include/freetype2
BULLET_PATH = /usr/local/include/bullet
INC_PATH := -I$(SRC_NAME) -I$(HDR_PATH) -I$(FT_PATH) -I$(BULLET_PATH)

# Library files
LDFLAGS = 
LIB_PATH := -L../support/lib

# CMD settings
CMD_MK_FOLDER = mkdir
CMD_FIND =

# Linux
ifeq ($(OS),Linux)
	NPROCS:=$(shell grep -c ^processor /proc/cpuinfo)
	LDFLAGS += -lcupcake -lglfw3 -lGL -lGLEW -lX11 -lXrandr -lXi -lXxf86vm \
			   -lfreetype -lz -l:libfmodex64.so \
			   -lBulletDynamics -lBulletCollision -lLinearMath

	SRC_DIR := $(filter-out $(EXCLUDE), \
		$(shell find $(ROOT_DIR)/$(SRC_NAME) ! -name "src" -type d -print))
	OUT_DIR := $(subst $(SRC_NAME),$(OUT_NAME), $(SRC_DIR))
	SRC := $(shell find $(ROOT_DIR)/$(SRC_NAME) -name "*.cpp")
endif
# OSX
ifeq ($(OS), OSX)
	NPROCS:=$(shell system_profiler | awk '/Number Of CPUs/{print $4}{next;}'}}})
endif

# Makeflags
MAKEFLAGS += -j$(NPROCS)

# Files
OBJ := $(subst $(SRC_NAME),$(OUT_NAME),$(patsubst %.cpp,%.o, $(SRC)))

# Output settings
EXEC = Cron

all: $(OUT_DIR) $(EXEC)

$(EXEC): $(OBJ) 
	$(CC) -o $@ $(subst src,.,$^) $(CCFLAGS) $(DDFLAGS) $(INC_PATH) \
		$(LIB_PATH) $(LDFLAGS)

$(OUT_DIR) :
	@$(CMD_MK_FOLDER) -p $@

$(OUT_NAME)/%.o : $(SRC_NAME)/%.cpp
	$(CC) -c -o $(subst src,.,$@) $< $(CCFLAGS) $(DDFLAGS) $(INC_PATH) \
		$(LIB_PATH) $(LDFLAGS)

clean :
	-rm -rf $(EXEC)
	-rm -rf $(OUT_NAME)
	@echo Clean done

debug:
	$(info $$SRC_DIR is $(SRC_DIR))
	$(info $$OUT_DIR is $(OUT_DIR))
	$(info $$SRC is $(SRC))
	$(info $$OBJ is $(OBJ))

.PHONY: all clean debug
