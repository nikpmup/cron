#version 120

uniform mat4 uModelMat;
uniform mat4 uViewMat;
uniform mat4 uProjMat;

attribute vec3 aPosition;

void main() {
    gl_Position = uProjMat * uModelMat * uViewMat * vec4(aPosition, 1.0f);
}