#version 120

uniform sampler2D uTexID;
uniform vec3 uDiffuse;

varying vec2 vTexCoord;

void main(void) {
    gl_FragColor = vec4(uDiffuse, texture2D(uTexID, vTexCoord).r);
}