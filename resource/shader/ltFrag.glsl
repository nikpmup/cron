#version 120

uniform vec3 uColor;
uniform vec3 uLightPos;
uniform vec3 uLightColor;
uniform float uLightIntensity;

uniform sampler2D uTexID;

uniform vec3 uAmbient;
uniform vec3 uDiffuse;
uniform vec3 uSpecular;
uniform float uReflect;

varying vec3 vPosWorld;
varying vec3 vPos;
varying vec3 vNorm;
varying vec3 vCam;
varying vec3 vLight;
varying vec2 vTexCoord;

void main() {
    // Distance of light
    float distance = length(uLightPos - vPosWorld);

    // Normal
    vec3 norm = normalize(vNorm);
    // Light
    vec3 light = normalize(vLight);
    // Angle
    float cosTheta = clamp(dot(norm, light), 0.0f, 1.0f);

    // Eye
    vec3 eye = normalize(vCam);
    // Reflection
    vec3 refl = reflect(-light, norm);
    // Angle
    float cosAlpha = clamp(dot(eye, refl), 0.0f, 1.0f);

    // Color
    vec3 color = uAmbient + uDiffuse * uLightColor * uLightIntensity* cosTheta +
                 uSpecular * uLightColor * uLightIntensity * pow(cosAlpha, uReflect) ;
    
    gl_FragColor = texture2D(uTexID, vTexCoord) * vec4(color, 1.0f);
}
