#version 120

uniform mat4 uModelMat;
uniform mat4 uViewMat;
uniform mat4 uProjMat;
uniform mat4 uNormMat;
uniform vec3 uLightPos;


attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;

varying vec3 vPosWorld;
varying vec3 vPos;
varying vec3 vNorm;
varying vec3 vCam;
varying vec3 vLight;
varying vec2 vTexCoord;

void main() {
    // Position in world space
    vPosWorld = (uModelMat * vec4(aPosition, 1.0f)).xyz;
    
    vec4 posCamSpace = uViewMat * uModelMat * vec4(aPosition, 1.0f);
    
    // Camera vector 
    vCam = -posCamSpace.xyz;
    
    // Light vector
    vLight = (uViewMat * vec4(uLightPos, 1.0f)).xyz + vCam;
    
    // Normal vector
    vNorm = (uNormMat * vec4(aNormal, 0.0f)).xyz;
    
    // Tex vector
    vTexCoord = aTexCoord;

    // Sets position in scene
    gl_Position = uProjMat * posCamSpace;
}